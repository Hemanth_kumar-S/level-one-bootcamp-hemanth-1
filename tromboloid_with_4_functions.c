//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>
float input()
{
    float  p;
    
    printf("Enter  the  Value:");
    scanf("%f",&p);
    
    return p;
}
float find_vol(float p,float q,float r)
{
    float vol;
    vol=((p*q*r)+q/r)/3;
    
    
    return vol;
}
void output(float p,float q,float r,float vol)
{
    printf("The Volume of  tromboloid  with h=%f,d=%f and b=%f is %f",p,q,r,vol);
    
}
int main()
{
    float h,d,b,v;
    
    h=input();
    d=input();
    b=input();
    v=find_vol(h,d,b);
    output(h,d,b,v);
    
    return 0;
}