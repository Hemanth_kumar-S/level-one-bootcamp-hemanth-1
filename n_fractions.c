//WAP to find the sum of n fractions.

#include<stdio.h>
struct fract
{
int numer;
int denom;
};
typedef struct fract fraction;
fraction input()
{
fraction a;
printf("Enter the numerator\n");
scanf("%d",&a.numer);
printf("Enter the denominator\n");
scanf("%d",&a.denom);
return a;
}
fraction sum(fraction a,fraction b)
{
fraction result;
if(a.denom==b.denom)
{
result.denom= a.denom;
result.numer=a.numer+b.numer;
}
else
{
result.denom=a.denom*b.denom;
result.numer=(a.numer*b.denom)+(b.numer*a.denom);
}
return result;
}
int main()
{
	int n;
	printf("Enter the number of fractions to calculate\n");
	scanf("%d",&n);
	fraction c,a[n];
	c.numer=0;
	c.denom=1;

for(int i=0;i<n;i++)
{
printf("For the fractions  %d\n",(i+1));
a[i]=input ();
}
for(int i=0;i<n;i++)
{
c=sum(c,a[i]);
}
printf("The final sum is %d/%d is %.2f",  c.numer  , c.denom,  (c.numer/(1.0*c.denom)));
return 0;
}
