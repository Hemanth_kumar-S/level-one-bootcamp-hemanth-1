//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>
int input()
{
    int k ;
    
    printf("Enter Number:");
    scanf("%d",&k);
    
    return k;
}
int find_sum(int k,int v)
{
    int sum;
    sum=k+v;
    
    return sum;
}
void output(int k,int v,int sum)
{
    printf("Sum of the %d and %d is %d",k,v,sum);
    
}
int main()
{
    int p,q,s;
    
    p=input();
    q=input();
    s=find_sum(p,q);
    output(p,q,s);
    
    return 0;
}
