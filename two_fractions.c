//WAP to find the sum of two fractions.

   #include<stdio.h>
	typedef struct fraction
	{
 	int num;
	int deno;
          }Fraction;

	Fraction input()
	{
		Fraction f;
		printf("\n Enter the numerator:");
		scanf("%d",&f.num);
		printf("Enter the denominator:");
		scanf("%d",&f.deno);
		return f;
 	}
	int hcf(int a,int b)
	{
	   int i,gcd=1;
	   for(i=2;i<=a && i<=b;++i)
	   {
               if(a%i==0 && b%i==0)
		gcd=i;
 	   }
	     return gcd;
        }
         Fraction sum(Fraction f1,Fraction f2)
        {
		int gcd;
		Fraction temp;
		temp.num=((f1.num*f2.deno)+(f2.num*f1.deno));
		temp.deno=(f1.deno*f2.deno);
		gcd=hcf(temp.num,temp.deno);
		temp.num=temp.num/gcd;
		temp.deno=temp.deno/gcd;
		return temp;
       }
        void result(Fraction f1,Fraction f2, Fraction sum)
	{
	          printf("The Sum of the %d /%d + %d/%d  is %d/%d",f1.num,f1.deno,f2.num,f2.deno,sum.num,sum.deno);
	}
           int main()
            {
		Fraction f1,f2,res;
		printf("Enter numerator and denominator of 1st fraction:");
		f1=input();
		printf("Enter numerator and denominator of 2nd fraction:");
		f2=input();
		res=sum(f1,f2);
		result(f1,f2,res);
 		return 0;
	}
